var {src, dest, watch, series, parallel} = require('gulp'); 
var sass = require('gulp-sass');
var pug = require('gulp-pug');
var browserSync = require('browser-sync').create();
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');


// file path
const  files  = {
    scssPath: './assets/scss/**/*.scss',
    pugPath: './views/**/*.pug'
}

// Static server
function browserSyncTask(){
    browserSync.init({
       proxy:  "localhost/webtoolkit/src",
       port: 8080
    });
}

// Scss Task
function scssTask() {
    return src(files.scssPath)
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(dest('./assets/css/'))
        .pipe(browserSync.stream());
};

// pug Task
function pugTask(){
   return  src('./views/**/*.pug')
    .pipe(pug({
        doctype: 'html',
        pretty:true
    })) 
    .pipe(dest('./src/'))
    .pipe(browserSync.stream());
};

// Watch Task
function watchTask(){

    browserSync.init({
        proxy:  "localhost/webtoolkit/src",
        port: 8080
     });

    watch([files.scssPath, files.pugPath],
        parallel(scssTask,pugTask)
    );

   // watch("./src/*.html", reload);

}

// Default Task

exports.default = series(
    parallel(scssTask,pugTask),
    watchTask
) 


// sass.compiler = require('node-sass');


// //sass task
// gulp.task('sass', function () {
//     return gulp.src('./assets/scss/**/*.scss')
//         .pipe(sourcemaps.init())
//         .pipe(sass().on('error', sass.logError))
//         .pipe(cleanCSS({compatibility: 'ie8'}))
//         .pipe(gulp.dest('./assets/css/'));
// }); 

// //pug taskk
// gulp.task('pug', function(){
//    return  gulp.src('./views/**/*.pug')
//     .pipe(pug({
//         doctype: 'html',
//         pretty:true
//     })) 
//     .pipe(gulp.dest('./src/'));
// });

// gulp.task('concat', gulps.series('clean', function() {
//     return gulp.src('./assets/js/**/*.js')
//     .pipe(concat('all.js'))
//     .pipe(gulp.dest('./assets/js/'))
//   }));

 

// // watch task
// gulp.task('watch',function() { 
//     gulp.watch('./views/**/*.pug', gulp.series('pug'));
//     gulp.watch('./assets/scss/**/*.scss', gulp.series('sass'));
//     gulp.watch('./assets/js/**/*.js', gulp.series('concat'));
// });

////gulp.task('build', gulp.parallel('concat', 'watch'));

//gulp.task("default",  gulp.parallel('build'));